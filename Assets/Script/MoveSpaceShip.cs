﻿using Packages.Rider.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpaceShip : MonoBehaviour
{

    public Rigidbody GetPhysics
    {
        get
        {
            return rigidbody;
        }
    }

    [SerializeField] private Transform transformShip;

    [SerializeField] private Rigidbody rigidbody;

    [SerializeField] private bool interialDamper = true;

    [Range(0, 100)]
    [SerializeField] private float forceInterailDamper;

    [Range(0, 1000)]
    [SerializeField] private float forceRotateInterailDamper;

    [Range(0, 1000)]
    [SerializeField] private float maxPower;

    [Range(0, 100)]
    [SerializeField] private float speedRotate;

    [SerializeField] private float power;

    private const float multiplication = 150f;


    private void Update()
    {
        if (Input.GetKey(KeyCode.G))
        {
            power -= multiplication * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.T))
        {
            power += multiplication * Time.deltaTime;
        }
        power = Mathf.Clamp(power, 0, maxPower);

        rigidbody.AddForce(transformShip.forward * power);

        int x = 0, y = 0, z = 0;

        if (Input.GetKey(KeyCode.W))
        {
            x = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            x = -1;
        }

        if (Input.GetKey(KeyCode.A))
        {
            y = -1;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            y = 1;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            z = 1;
        }
        else if (Input.GetKey(KeyCode.E))
        {
            z = -1;
        }

        rigidbody.AddTorque(transformShip.TransformDirection(new Vector3(speedRotate * x, speedRotate * y, speedRotate * z)));

        if (interialDamper)
        {
            Vector3 local_velocity = transformShip.InverseTransformDirection(rigidbody.velocity);
            Vector3 local_angular_velocity = transformShip.InverseTransformDirection(rigidbody.angularVelocity);

            Vector3 invert_local_velocity = -local_velocity * forceInterailDamper;
            if (power != 0)
            {
                invert_local_velocity.z = 0;
            }

            Vector3 invert_local_angular_velocity = -local_angular_velocity * forceRotateInterailDamper;
            if (y != 0)
            {
                invert_local_angular_velocity.y = 0;
            }
            if (x != 0)
            {
                invert_local_angular_velocity.x = 0;
            }
            if (z != 0)
            {
                invert_local_angular_velocity.z = 0;
            }

            rigidbody.AddForce(transform.TransformDirection(invert_local_velocity));
            rigidbody.AddTorque(transformShip.TransformDirection(invert_local_angular_velocity));
        }
    }
}
