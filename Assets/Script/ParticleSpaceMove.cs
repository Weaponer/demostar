﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpaceMove : MonoBehaviour
{
    [SerializeField] private Camera camera;

    [SerializeField] private ParticleSystem effect;

    [SerializeField] private Material material;

    [SerializeField] private Vector3 directToWorld = Vector3.forward;

    [SerializeField] private AnimationCurve curveSpeed;

    [SerializeField] private MoveSpaceShip ship;

    private const float dist_effect = 50f;



    private void Awake()
    {
        effect.transform.forward = directToWorld;
    }

    private void LateUpdate()
    {
        if (ship)
        {
            Transform transform = effect.transform;
            transform.position = camera.transform.position - (directToWorld * dist_effect);

            material.color = new Color(1, 1, 1, 1 * (Mathf.Max(0, curveSpeed.Evaluate(Vector3.Dot(-directToWorld.normalized, ship.GetPhysics.velocity.normalized) * ship.GetPhysics.velocity.sqrMagnitude))));
        }
    }
}
