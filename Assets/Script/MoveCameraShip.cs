﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCameraShip : MonoBehaviour
{
    [SerializeField] private Transform posCamera;
    [Range(0, 280)]
    [SerializeField] private float SpeedRotate;

    private float rot_x;
    private float rot_y;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void OnEnable()
    {
        posCamera.localEulerAngles = Vector3.zero;
        rot_x = 0;
        rot_y = 0;
    }

    private void Update()
    {
        rot_y -= Input.GetAxis("Mouse Y") * SpeedRotate * Time.deltaTime;
        rot_x += Input.GetAxis("Mouse X") * SpeedRotate * Time.deltaTime;
        rot_y = Mathf.Clamp(rot_y, -90, 90);
        posCamera.localEulerAngles = new Vector3(rot_y, rot_x, 0);
    }
}
